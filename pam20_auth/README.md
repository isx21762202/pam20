# PAM

## Javier Moyano - isx21762202

ASIX M06-ASO 2020-2021

### Imágenes

* **sx21762202/pam20:ldap** host pam per practicar *PAM* amb autenticació local
  unix (*pam_unix.so*) i autenticació LDAP (amb *pam_ldap.so*). Utilitza el paquet
  *nss-pam-ldapd*. Cal configurar: *ldap.conf*, *nslcd*, *nscd* i *nssitch*.
  L'autentitació es configura al *system-auth*.


  Atenció, cal usar en el container --privileged per poder fer els muntatges nfs.

```
$ docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisix -p 389:389 -d isx21762202/ldap20:latest 
$ docker run --rm --name pam.edt.org --hostname pam.edt.org --net 2hisix --privileged -it isx21762202/pam20:ldap
```

